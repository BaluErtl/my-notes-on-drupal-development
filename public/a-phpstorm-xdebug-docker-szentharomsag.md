# A PhpStorm + Xdebug + Docker szentháromság

Szóval 2023. szeptember végén-október elején – a _Purge_ modul Drush-parancsának refaktorálása (DIT-1330) során – hosszú napokat szopódtam vele, ezért végre nekiállok, hogy egyszer és mindenkorra megértsem a PhpStorm + Xdebug + Docker szentháromság működését.

Ehhez először is azt kellene megértenem, hogyan működik a PhpStorm együtt a Dockerrel? Ezért aztán csináltam egy végtelenül egyszerű kis projektet, ami semmi mást nem tartalmaz, csak egy `appserver` konténert (lásd a Landó PHP szervizének [doksiját](https://docs.lando.dev/php/config.html)), amiben fut egyetlen árva `index.php` fájl – még a [LAMP](https://docs.lando.dev/lamp/) recipét is soknak éreztem hozzá).

```yaml
# .lando.yml

name: egy-sima-php-app
services:
  appserver:
    type: php:8.2
    xdebug: "debug,develop"
    overrides:
      environment:
        # Ez a legalapabb szintu definicioja az erteknek.
        # See: https://3.xdebug.org/docs/all_settings#idekey
        DBGP_IDEKEY: PHPSTORM
xdebug: true
```
```php
# index.php

<?php

$c = NULL;
$a = 4;
$b = 11;

function szorozz($ezt, $ezzel): int {
  return $ezt * $ezzel;
}

$c = szorozz($a, $b);

echo "$c\n\n";
```

1. Mielőtt bármit elkezdenénk a PS-ben mókolni, ellenőrizzük, hogy biztosan hozzá van-e adva az appunk forráskódjának gyökérkönyvtára „content root”-ként a PS-projekthez. Hátrányosan érinti ugyanis a hibakeresés futtatását, ha ez itt nem jól van beállítva: ![Set project directory as content root](images/set-project-directory-as-content-root.png "")
De ha már itt vagyok, akkor elolvasom az idevágó [doksit](https://www.jetbrains.com/help/phpstorm/2023.2/content-root.html) és megértem ezt is, mert valószínűleg fontos lesz máshol is:
    - Alapesetben minden mappa szürke (![Grey folder icon](https://resources.jetbrains.com/help/img/idea/2023.2/app.nodes.folder.svg "") ), ezért ezzel sajnos sok újat nem mond a doksi
    - **Source roots:** a világoskék (![Light blue folder icon](https://resources.jetbrains.com/help/img/idea/2023.2/app.modules.sourceRoot.svg "") ) mappa ikon a névterekkel van összefüggésben. Ezek alapján javasol könyvtárneveket, amikor osztályokkal dolgozunk.
    - **Resource roots:** a halványlila (![Light purple folder icon](https://resources.jetbrains.com/help/img/idea/2023.2/app.icons.resourceRoot.svg "") ) mappa ikon csak annyit csinál, hogy engedi ezeket a fájlokat relatívan hivatkozni (nem teljesen értem?)
    - **Excluded roots:** a narancssárga (![Light orange folder icon](https://resources.jetbrains.com/help/img/idea/2023.2/app.modules.excludeRoot.svg "") ) mappa ikonnal jelölt fájlokat nem indexeli, így nem is kereshetőek,nem parse-olja, nem figyeli a változásaikat, stb. Tipikusan ideiglenes artifact build-ek, naplókimenetek, DB-dumpok, stb.
    - **Test source roots:** értelemszerűen a tesztek fájljai vannak zölddel (![Light green folder icon](https://resources.jetbrains.com/help/img/idea/2023.2/app.modules.testRoot.svg "") ) jelölve.
1. Szintén hasznos lesz később, ha még az elején beállítjuk a deploy szervert a projekthez: ![Define a deployment server for the project](images/define-a-deployment-server-for-the-project.png "")
1. `$ docker ps`-sel megnézem a futó konténereket és az `appserver` imidzsének a nevét kimásolom (pl. `devwithlando/php:8.2-apache-4`)
1. `$ docker image prune -y` ([doksi hozzá](https://docs.docker.com/engine/reference/commandline/image_prune/)) paranccsal kitakarítom a használaton kívüli imidzseket, hogy a PS adminfelületén majd könnyebb legyen megtalálni.
1. Most beállítom a PS-ben a _CLI interpretert_ így. A sárgával jelölteket érdemes összehasonlítani a konténerbe belépve egy `$ php -v` parancs eredményével. Ha nem jelennek meg ezek az értékek a felületen, akkor nem sikerült a PS-nek kapcsolódnia a konténerhez: ![PhpStorm's general PHP settings for a simplest app run in Docker](images/phpstorm-cli-interpreter-settings-for-devwitlando-php-image.png "")
1. Ezután megadom a PS-projekt általános PHP-beállításait is így: ![PhpStorm's general PHP settings for a simplest app run in Docker](images/phpstorm-general-php-settings-for-a-simplest-app-run-in-docker.png "")
1. Ha most futtatom a szkriptet a PS-ből, akkor meg kell találnia az interpretert és le kell futnia a kódnak, végül megjelenik a „Process finished with exit code 0” üzenet, ami jó: ![Running a PHP script successfully](images/running-php-script-successfully.png "")
1. Ekkor már elvileg működnie kell a hibakeresésnek is: ![Debugging a simple PHP script](images/debugging-a-simple-php-script.png "")
  Eddig tehát annyit értünk el, hogy a PS tudja közvetlenül hivogatni a konténeren belül futó PHP-binárist, valamint eléri annak Xdebug-bővítményét is, ami szuper. De eddig nem volt webszerver a képben.

Szóval először elkezdtem olvasni a böngészőkkel való összekötését: az [idevonatkozó doksiból](https://www.jetbrains.com/help/phpstorm/2023.2/settings-tools-web-browsers.html) megtudtam, hogy ez csak HTML és XML fájlokra vonatkozik (PHP-ra nem). Ha meg akartam nyitni egy `index.html` fájlt, akkor nagyon rossz helyen kereste az URL-ben:

| http://localhost:63342/egy-sima-php-app/egy-sima-php-app/index.php?_ijt=dgh9rriu6qjhe8v9bqsgklh73q&_ij_reload=RELOAD_ON_SAVE |
|------------------------------------------------------------------------------------------------------------------------------|

Innen kezdett gyanússsá válni, hogy nem nagyon van tisztában a projekt docroot-jával. Szóval csináltam egy szerver definíciót a projekthez: ![Define a debug server for the project](images/define-a-debug-server-for-the-project.png "")

Ahogy olvasom azonban a szerver definíciók [doksiját](https://www.jetbrains.com/help/phpstorm/2023.2/servers.html). Fontos tudni, hogy két különböző „szerver” fogalmat használ a PS adminfelülete és doksija: vannak „debug szerverek” (amik a helyi PHP-környezethez, CLI Interpreterhez állnak közel) és vannak a „deployment szerverek” ([doksi](https://www.jetbrains.com/help/phpstorm/2023.2/configuring-synchronization-with-a-remote-host.html)) – a különbözőségükről [itt írnak](https://www.jetbrains.com/help/phpstorm/2023.2/creating-a-php-debug-server-configuration.html#deddcc7d) még.

Kiderült, hogy ezeknek a PHP-környezet szervereknek csak az alábbi három hibakeresési üzemmód esetén van haszna:
  1. PHP Web Page
  1. PHP Remote Debug
  1. Zero-Configuration Debugging

Ezek már ismerősen csengtek valahonnan, de nem emlékeztem, hogy honnan. Némi keresgélés után meg is lett. Ilyet a _Run_ ▹ _Edit Configurations…_ menüpont alatt lehet felvenni a listából ([doksi](https://www.jetbrains.com/help/phpstorm/2023.2/creating-php-web-application-debug-configuration.html)): ![Add Run/Debug Configuration for a given type of files](images/add-run-debug-configuration-for-a-given-type-of-files.png "")

Ilyenkor az `index.php` fájl szerkesztőablaka fölött a bogár ikonra kattintva valóban meghívja az alapértelmezett böngészőt, betölti a helyes doménnéven a fájl elérési útvonalát és rögtön fel is függeszti a futását, hogy lehessen léptetgetni a lefutását. Ez eddig szuper, hogy rájöttem, eddig hogyan működik. Sőt, azt is kipróbáltam, hogy a böngészőbővítmény felől indítva is működik (bármiféle „my-ide” session-azonosító nélkül): telefon ikon PS-ben, bogár ikon a böngészőben, `index.php` oldal újratöltése és szépen várakozik a léptetgetésre.

Akkor most értsük meg, mit csinál a másik két hibakeresési üzemmód. Sokat hivatkozza a „Zero-Configuration Debugging” kifejezést, én innentől csak ZCD-ként rövidítem. Mint azt a különböző hibakeresési üzemmódok összefoglaló [oldalának](https://www.jetbrains.com/help/phpstorm/2023.2/php-debugging-session.html) első pár bekezdéséből megtudtam, a ZCD tul.képp nem más, mint az előbbi _Run/Debug Configuration_ funkcionalitás nélküli indítása a hibakeresésnek. Tovább olvasva kezdett rémleni, hogy hasonlót már olvastam nemrég a VS Code [doksijában](https://code.visualstudio.com/docs/editor/debugging#_launch-versus-attach-configurations):

  - **Launch:** az IDE indítja el a kód futását és nyitja meg hozzá a böngészőt, miközben a debugger a háttérben figyel. Másképpen: recept a kód debug üzemmódban való elindításához, majd csatlakozik hozzá az IDE. A PS-ben ez akkor az első két üzemmód, ha jól értem.
  - **Attach:** például a megnyitott böngészőfülön már futó JS-kódhoz csatolódik hozzá a DevTools, mint debugger. Másképpen: recept arra, hogyan csatlakoztassuk az IDÉ-t egy már futó kódhoz. A PS-ben ez pedig a ZDC kizárásos alapon.

  Mindkét esetben eltérő paraméterek szükségesek, ezért nem mindegy, hogy melyiket hogyan állítjuk be.

Mielőtt ZDC üzemmódban akarnánk debuggolni, mindenképp érdemes ellenőrizni a szerverbeállításokat a _Run_ ▹ _Web Server Debug Validation_ funkcióval. Mivel a lépéssor elején már definiáltunk _deploy_szervert_, így a PS már tisztában van vele, hogy hol fogja tudni HTTP-n keresztül elérni a már futó projektünket, tehát oda tudja másolni ideiglenesen a saját kis validátor szkriptjét és az abból kapott eredménnyel tér vissza (előtte a telefonkagyló ikonnal engedélyezni kell a hallgatást): ![Results of debug server configuration validation with deploy server defined](images/results-of-debug-server-configuration-validation-with-deploy-server.png "")

Ha azonban próbaképpen bemásoljuk a PHP-konténerben kiadott `$ php -i` parancs kimenetét az első opciónál, akkor eltérő lesz a „Discover client host” sor eredménye: ![Results of debug server configuration validation by phpinfo() output](images/results-of-debug-server-configuration-validation-with-phpinfo.png "")

Egyelőre nem világos számomra, hogy melyiknek higgyek. De mindenesetre most a böngészőben újratöltve az oldalt valóban érzékeli a hibakeresési kérést a PS.

Most egyelőre úgy érzem, hogy ez már elég infó amit kipróbálhatnék egy nagyobb projekten (ha nagyon elakadnék, akkor még mindig ott a köztes szint, a [LAMP](https://docs.lando.dev/lamp/) recipe). Először a Wodby-alapú _D10 Vanilla_ projekttel kezdtem, de már a **deploy szerver** definiálásánál elakadtam, úgyhogy inkább mentem tovább _site-for-acquia_purge_ projektre, ami legalább Landós. Itt némi próbálkozást követően valóban sikerült végre Drupal-kódot léptetgetnem, aminek most elmondhatatlanul örülök.

A nap azonban telik, szóval ha fájó szívvel is, de egyelőre ezt most itt abbhagyom, majd [ettől a fejezettől](https://www.jetbrains.com/help/phpstorm/2023.2/zero-configuration-debugging.html) fogom folytatni ha lesz időm újra.


## Haladó szint: CLI-ban futó PHP-szkriptek hibakeresése:
A ZCD-üzemmód [alfejezete](https://www.jetbrains.com/help/phpstorm/2023.2/zero-configuration-debugging-cli.html), hogyan lehet ugyanezt nem böngészőből, hanem CLI-ból elindítani, ha mondjuk a projektgyökérbe teszünk egy kis próbaszkriptet:
1. `$ chmod u+x ./szevasz.php`
1. `$ php -dxdebug.mode=debug -dxdebug.client_host=127.0.0.1 -dxdebug.client_port=9003 -dxdebug.start_with_request=yes -f ./szevasz.php`

Ha mélyebbre teszem valahová, akkor nem találja/tudja PHP-bináris lefuttatni, egyelőre hagytam, elég gyökérből tesztelni. Hogy ne kelljen folyamatosan kopipésztelni: `$ alias ezt='php -dxdebug.mode=debug -dxdebug.client_host=127.0.0.1 -dxdebug.client_port=9003 -dxdebug.start_with_request=yes -f ./szevasz.php'`

Ez a [fejezet](https://www.jetbrains.com/help/phpstorm/2023.2/debugging-a-php-cli-script.html) szinte ugyanazt írja le, hogyan lehet akár a ZCD-től függetlenül, közvetlenül a CLI-ből indítani a hibakeresést; akár a GUI-ról.

De hogy haladjunk a feladatom felé, még Drupal-specifikusabban [doksi](https://www.jetbrains.com/help/phpstorm/2023.2/drupal-support.html#debugging-drush-commands)

[Ezt még esetleg hozzáolvasni](https://www.jetbrains.com/help/phpstorm/2023.2/drush.html)



## Böngészőbővítmény kell-e vagy sem, ha igen, melyik?

Az „Xdebug helper” nevű Chrome-bővítmény ([Web Store](https://chrome.google.com/webstore/detail/xdebug-helper/eadndfjplgieldjbigjakmdgkmoaaaoc?authuser=1)) tekinthető az eredetinek, a [mac-cain13/xdebug-helper-for-chrome](https://github.com/mac-cain13/xdebug-helper-for-chrome) repóból készül és a JetBrains [hivatalos doksija](https://www.jetbrains.com/help/phpstorm/2023.2/browser-debugging-extensions.html) is ezt ajánlja.

A másik, megszólalásig hasonló „Xdebug Chrome Extension” ([Web Store](https://chrome.google.com/webstore/detail/xdebug-chrome-extension/oiofkammbajfehgpleginfomeppgnglk?authuser=1)) az a [mydogger/xdebug-helper-for-chrome](https://github.com/mydogger/xdebug-helper-for-chrome) repója szerint is csak egy forkja az előbbinek. Álltítólag abban jobb, hogy már támogatja az MV3-as manifesztet.
