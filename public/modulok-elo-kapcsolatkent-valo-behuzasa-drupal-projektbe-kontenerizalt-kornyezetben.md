# Modulok élő kapcsolatként való behúzása a Drupal-projektbe konténerizált környezetben

Ez a téma először a konténerizációs topik egyik alszakasza volt, de inkább kihoztam onnan, mert annál fontosabb. A kontrib modulok fejlesztéséhez (az Acquia-meló erről szól) elengedhetetlen, hogy a helyi webhelypéldányba dinamikusan legyen behivatkozva a modul forráskódja. Erre több módszert is kipróbáltam. Ha csak a végső megoldás érdekel, akkor ugorj az „Éjjel 23:23-kor végre működik, ahogy mindig is szerettem volna” szakaszhoz. Ha a felfedezés folyamatának történeti háttere is érdekel, akkor folytasd az olvasást.

## Kézzel symlink-elés
Egyszer csináltam így: `$ ln -s /Users/baluertl/Repos/modul_neve /Applications/MAMP/htdocs/webhelyneve/modules/contrib`, ekkor bár létrehozta ugyan a symlink-hivatkozást, de a Drupal ettől még nem látta a modult. Feltételezésem szerint valószínűleg azért, mert ez a kézi parancs csak a gazdagép kontextusában lett kiadva, bent a konténerizált környezetben ennek semmilyen hatása nem volt.

## Ha csak Docker Compose van
Ha csak simán a korábban natívan használt módon (azaz a `composer.json`-ben „path” típusú repóként definiálva, majd úgy beigényelve a csomagot) akarunk élő, dinamikusan frissülő kapcsolatot teremteni a webhely kódbázisa és mondjuk egy kontrib modulnak a gépünkön valahol máshol tárolt kódbázisa között, akkor az konténerizálva nem fog menni elsőre. Ugyanis a PHP konténerben futó Composer szkript nem fog „kilátni” a mi gazdagépünkre.

Előtte ugyanis fel kell mountolni (azt hiszem, ez a „bind” típusú), az adott modul helyi kódbázisának könyvtárát a `docker-compose.yml` fájlban a `php` szervíz (vagy bármi is a neve a kódot futtató konténernek) definíciójánál így:

```yaml
# docker-compose.yml fájl

services:
  php:
    volumes:
      - ../../../../Users/baluertl/Repos/modul_neve:/var/www/html/web/modules/contrib/modul_neve    # <-- nem kell a `:cached` a végére
      - ./:/var/www/html:cached
```
| **FONTOS!** |
|:------------|
| _A kötetek felsorolásakor mindig a `./:/var/www/html:cached` legyen az utolsó, máskülönben az alkalmazás hosztnevére csak egy sima „404 page not found” hibaüzenet lesz az eredmény a böngészőben._ |

Ilyenkor elvileg megjelenik az adott modul könyvtárja, de előfordulhat, hogy a Finder-ben üresnek látszik. Sose hagyatkozzunk a Finder-re, mindig lépjünk be a konténerbe CLI-ben és ott ellenőrizzük a valóságot. Érdemes a modul infófájljában a cím módosításával csekkolni, hogy valóban azonnal látjuk-e a Drupal adminfelületén a változást.

## Ha Lando is van
Ha Landót használ a projekt, akkor nincs `docker-compose.yml` fájl. Mivel a [Landó doksija](https://docs.lando.dev/core/v3/services.html#overrides) szerint a `.lando.yml` végső soron úgyis egy `docker-compose.yml` fájllá fordul le, ezért lehetőség van annak a legfelsőszintű `services:` kulcsa alatti beállításokat felülírni. Mást, pl. network-öt nem lehet, de nekünk ez pont elég:

```yaml
# .lando.yml file

name: site-for-acquia-search
recipe: drupal10
config:
  webroot: ./docroot
plugins:
  "@lando/drupal": ./../../../

services:
  appserver: # <-- Lando esetén máshogy hívják a kódot futtató konténert.
    overrides:
      volumes:
        - ../../../../Users/baluertl/Repos/modul_neve:/app/web/modules/contrib/modul_neve    # <-- nem kell a `:cached` a végére
```
Így tehát felcsatlakoztattuk a fejlesztés alatt álló modulnak a gazdagépen valahol máshol tárolt kódját a projektet futtató konténerbe. Ezután `$ lando rebuild -y` és érdemes ilyenkor is csekkolni egy infófájl-módosítással, hogy a Drupal látja-e a könyvtárban az infófájlt.


## A Composer „értesítése”
Először is csekkoljuk, hogy az éppen kontribolni kívánt modulnak van-e, és ha igen, mennyi külső függősége. Ha nincs neki, vagy egyértelműen látszik, hogy azok teljesülnek (pl. PHP verzió), akkor ez a szakasz simán kihagyható. A Drupalnak úgyis csak annyi a lényeg, hogy ott legyen az adott könyvtárban az infófájl és a kódfájlok.

Ha azonban sok külső dependenciája van (pl. más kontrib modulra, amik további csomagokat igényelnek), akkor az adminfelületen inaktív lesz a modulunk jelölőnégyzete, hiszen senki nem gondoskodott a függőségeinek előzetes telepítéséről, mivel az utóbbi két módszerrel a Composer tudta nélkül, annak megkerülésével tettük oda a modulok kódbázisát a `modules/` könyvtárba. Ilyenkor jön a régi, natív környezetben is megszokott ([leírás](https://medium.com/@sirajul.anik/composer-require-a-php-package-from-a-local-or-remote-repository-7e641bdbc824)) „path” típusú repóként való felvétel módszere a `composer.json` fájlban:

```json
[…]
    "repositories": {
        "drupal/modul_neve": {
            "type": "path",
            "url": "/app/web/modules/contrib/modul_neve"
        },
        "drupal": {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        }
    },
[…]
    "minimum-stability": "dev"
```

Mivel a Composer-t a konténeren belül fogjuk futtatni, ezért természetesen a belső elérési útvonalát (lásd a `.lando.yml` fájlban felmountolva) adjuk meg a modul könyvtárának. Nekem ez elsőre furának tűnt, hogy a forrás (repó) útvonala megegyezik a cél (`modules/` könyvtár) útvonalával, de a Composert láthatóan nem zavarja. Két fontos dolog:
  - Mivel az eddigi telepítési lépések során a Composer `minimum-stability` beállítása a legmagasabb „stable” értéken jött létre, ezért a repók prioritási sorrendje miatt panaszkodni fog a Composer, hogy csak az alsóbb szintű (hivatalos D.org/8) repóban talál ennek megfelelő csomagot, a magasabb szintű repóban lévőnek nem felel meg a stabilitása. Ezért kell ezt a legalacsonyabb „dev” értékre állítani.
  - Ha a `composer.json` fájlban korábban csak 1 db repó (valószínűleg a hivatalos D.org/8) szerepelt, akkor az általunk újonnan elé felvett másik repó miatt át kell írni a JSON-szintaxist `[]`-ról `{}`-ra és nevet kell adni a korábban már ott lévőnek.

Ha egy rendesen, netről letöltött modulra futtatjuk a `$ composer show drupal/modulneve --available` parancsot, akkor kapunk egy ilyen hasznos infóblokkot, amiből itt most nekünk a `source`, `dist` és `path` kezdetű sorok érdekesek:
```
[…]
source   : [git] https://git.drupalcode.org/project/admin_toolbar.git 3.4.2
dist     : [zip] https://ftp.drupal.org/files/projects/admin_toolbar-3.4.2.zip 3.4.2
path     : /app/web/modules/contrib/admin_toolbar
[…]
```

Ha ugyanezt egy, a fent leírt módon odacsinálkodott modulra futtatjuk, akkor ezek máshogy festenek:
```
name     : drupal/acquia_dam
descrip. : Integrates with Acquia DAM
keywords :
versions : 1.0.x-dev, dev-3384273-include-image-on
type     : drupal-module
license  : GNU General Public License v2.0 or later (GPL-2.0-or-later) (OSI approved) https://spdx.org/licenses/GPL-2.0-or-later.html#licenseText
homepage :
source   : []
dist     : [path] /app/temp/acquia_dam 8e14ddfd12e4c89b855008c2e6865510b9b8c28c
names    : drupal/acquia_dam
[…]
```

Ezután érdemes egy `$ lando composer update --lock` parancsot futtatni (ennek a konténeren belül kell lefutnia!), hogyha bármi problémája lenne az útvonallal, akkor az még most kiderüljön. Ha esetleg ezt a hibaüzit kapnánk:

> _„The `url` supplied for the path (/app/modules/contrib/modul_neve) repository does not exist”_

Semmi pánik, valószínűleg kívülről futtattuk a Composer-parancsot, aminek ugyebár semmi értelme, hiszen a gazdagépen nem létezik a repónál megadott útvonal. Vagy használjuk a „lando” előtagot a parancs előtt vagy SSH-zzunk be a konténerbe.

Ezután behúzzuk a csomagot a projektbe, de fontos a `--prefer-source` kapcsolót használni, hogy a fejlesztés alatt álló modul saját Git információit is hozza magával. Mivel valószínűleg egy D.org-os ticketen dolgozunk, aminek nagy eséllyel már van saját MR-je nyitva GitLabon, ezért rögtön az adott fork repóban lévő issueszámos ágat kérjük a Composertől, hogy telepítse:

```shell
$ lando composer require 'drupal/acquia_dam:3384273-include-image-on-dev@dev' --prefer-source
```

Ilyenkor az alábbiak egyikéhez hasonló sort kell látnunk:
```
  - Installing drupal/acquia_dam (dev-3384273-include-image-on): Symlinking from /app/modules/contrib/acquia_dam
```
Vagy:
```
  - Installing drupal/modul_neve (3.1.x-dev): Source already present
```

Ha mindent jól csináltunk, akkor így már engedélyezhető a modul, hiszen a Composer innentől gondoskodott a külső függőségeinek jelenlétéről. Extra tipp: a modul engedélyezésekor érdemes rögtön `-y` kapcsolót használni a felesleges kérdés megspórolására: `$ lando drush en modul_neve -y`.


## A Composer azonban veszélyes is lehet – ilyenkor ne piszkáljuk, ha nem muszáj
Jók ezek az automatizált eszközök, hogy a Git segítségével önállóan módosítanak fájlokat a gépünkön, de egyetlen hátrányuk, hogy törléskor nincs visszavonás – az oprendszer kukája nyilván semmiről nem tud, amit ezek csinálnak.

Egy kontrib fejlesztői környezetnek az előző szakaszban leírt előkészítése során csak a külső függőségek beszerzéséhez van szükség a Composer-re. Ha a fejlesztési folyamat során váltanunk kell az adott modul különböző ágai között, akkor ezeket már sose a Composer-rel próbáljuk meg csere-berélni, mert ezzel azt kockáztatjuk, hogy annak automatizált telepítési folyamatának lépései között a csomag (mi esetünkben a modul) fájljait (ami ugyebár a mi lokális repónk, benne módosított fájlok, git stash-ek, hasznos kommentek a hibakeresés kellős közepén, stb.) letörli. Megtörtént eset, szerencsére viszonylag olcsó tanulópénzt fizettem érte. Onnan tűnt fel, hogy üres a modul könyvtára, hogy PhpStorm-ban bezáródott az összes, korábban szépen megnyitva tartott fájl szerkesztője. Helyette a csomag saját szintjén Git-tel tudunk ágat váltani, amiről a Composernek nem is kell tudnia. Ha már a külső függőségek fel lettek telepítve, hagyjuk a Composer-t és mindig Git-tel váltogassunk az ágak vagy címkék (ha egy adott kiadás kell) között.



## Éjjel 23:23-kor végre működik, ahogy mindig is szerettem volna

| | | |
|-|-|-|
| | _A példákban vegyesen használom a `drupal/purge` és `drupal/acquia_purge` modulok neveit, de a lényeget ez nem befolyásolja._ | |

Az előző szakaszban leírtak némiképp „jereváni rádió”: van bennük némi igazság, de sajnos önmagában nem tökéletes megoldáshoz vezetnek.

Ahogy az már korábban is gyanús volt számomra, nem egyezhet meg a forrás és a cél hely, ahonnan és ahová a Composer mozgatja a csomagokat. Már akkor felmerült bennem az ötlet, hogy esetleg kellene valamiféle „kvázi-packagist”, ahová először felmountolom a gazdagépről az éppen fejlesztés alatt álló modulok kódbázisát, majd ebből a „tárházból” választja ki a konténeren belül futó Composer, hogy pontosan melyik csomagnak melyik verzióját telepítse.

Azt is sejtettem, hogy ebbe az irányba a Composer `repositories` beállítása felé kellene indulnom. Ezért aztán végre rászántam az időt és elolvastam elejétől végéig a [Repositories](https://getcomposer.org/doc/05-repositories.md) leírást, ami a 3 lehetséges repófajtát mutatja be (az általam eddig az „Andoros” időkből használt `"type": "path",` is létezik ([doksi](https://getcomposer.org/doc/05-repositories.md#path)), de nem említi közöttük). Először a `vcs` típussal indultam, majd egy egyszerű MAMP-os projekten tettem egy próbát `package` típusúval, de végül rájöttem, hogy amit keresek, az valószínűleg mindhárom közül a „legintelligensebb”, a `composer` típusú repó lesz. Most leírom, hogyan működik éppen a gépemen:

1. A gazdagép szintjén marad a `/Users/baluertl/Repos` ahogy eddig is, benne a rengeteg verziókövetett Drupal-modullal (túlnyomórészt, de azért akad köztük más is)
2. Erről a tárolóról a metainformációkat egy mellette lévő szinten elhelyezett `/Users/baluertl/_my_local_packagist` könyvtárban árválkodó egyetlen `packages.json` fájl tárolja. Ennek megértéséhez nagyon hasznos volt alaposan átolvasnom a Composer-nek a [repók működéséről](https://getcomposer.org/doc/05-repositories.md#package) szóló ismertetőjét, amiben a csomagok belső kezelését is leírja: három tényező (vendor/csomagnév + verzió + forrás) együttesen határozza meg, hogy pontosan honnan minek melyik változatát fogja lehúzni. A `packages.json` fájlban is látszik, hogy egy-egy objektum a harmadik szinten mind egy különálló egységet ír le önálló „verziószámonként” (ami nekünk itt most nem szám, hanem 2 külön lokális Git-ág) alábontva:
    ```json
    # packages.json fájl

    {
      "packages": {
        "drupal/acquia_purge": {
          "a-3397227-dorg-tickethez-keszult-ag": {
            "name": "drupal/acquia_purge",
            "type": "drupal-module",
            "version": "dev-3397227-update-to-3.5",
            "source": {
              "type": "git",
              "url": "./Repos/acquia_purge",
              "reference": "3397227-update-to-3.5"
            }
          },
          "az-en-titkos-proba-agam": {
            "name": "drupal/acquia_purge",
            "type": "drupal-module",
            "version": "dev-proba",
            "source": {
              "type": "git",
              "url": "./Repos/acquia_purge",
              "reference": "proba"
            }
          }
        }
      }
    }
    ```
3. A másik, itt most nagyon fontos tudnivaló a Composer repókról az a [sorrendiségük](https://getcomposer.org/doc/articles/repository-priorities.md#making-repositories-non-canonical) és a „canonical” funkció: biztonsági okokból (értelmesen meg van indokolva) a Composer v2 óta alapértelmezés szerint egy csomagot egyetlen repóból próbál meg kiszolgálni. Ha fentről lefelé haladva az egyikben megtalálja ugyan a vendor/csomagnév szerint egyező csomagot, de nem talál hozzá megfelelő verziót abban a repóban, akkor nem kutat tovább a többiben, hanem hibával megáll. Mivel nekem a saját helyi metaadat-nyilvántartásom kell, hogy előrébb szerepeljen a webhelyprojekt `composer.json` fájljában, mint a nyilvános repók, ezért alapértelmezett „canonical”-ként minden egyes fejlesztés alatt álló kontrib modulnak minden egyes ágát és címkéjét fel kellene vennem a fenti `packages.json` fájlomba, hogy később váltani tudjon közöttük (ha például egy függőség újabbat igényel, de én még nem írtam bele). Nem próbáltam, de _elvileg_ akár még működhetne is, hiszen nekem lokálban a teljes Git-repók megvannak, tehát az összerendelés működhetne. Sokkal egyszerűbb viszont, ha letiltom a „canonical”-ságát a helyi repómnak a `composer.json` fájlban (lásd a köv. pontban).
4. Merthogy mindez eddig csak a laptopomon a globális szinten zajlott, maga a webhelyprojekt Composer-e semmit nem tud még arról, hogy neki egy új repóban is keresnie kellene a gyári alapértelmezett Packagist-en kívül. Itt is új infó volt számomra ([doksi](https://getcomposer.org/doc/04-schema.md#repositories)), hogy „JSON object notation” formát lehet ugyan használni (szintén ahogy Andortól láttam annó), de nem tanácsos, mert nem garantált a repók felkutatási sorrendje. Ezért inkább maradok én is a szögletes zárójellel felsorolt változatnál:
    ```json
    # composer.json fájl

    […]
    "repositories": [
        {
            "type": "composer",
            "url": "/app/_my_local_packagist",
            "canonical": false
        },
        {
            "type": "composer",
            "url": "https://packages.drupal.org/8"
        }
    ],
    "require": {
    […]
    ```
5. Aki majd ezt a `composer.json` fájlt fel fogja nyalni, ő egy dobozban él, nem lát ki oda, ahol az én kontrib moduljaim vannak. Most jön képbe a konténerizáció. Ahhoz, hogy a konténeren belül futó Composer hozzáférjen ezekhez a konténeren kívül tárolt metaadatokhoz ÉS forráskódokhoz, mind a kettőt fel kell mountolni:
    ```yaml
    # .lando.yml fájl

    […]
    services:
      appserver:
        overrides:
          volumes:
            - ../../../../Users/baluertl/_my_local_packagist:/app/_my_local_packagist:cached
            - ../../../../Users/baluertl/Repos:/app/Repos:cached
    […]
    ```
6. Módosítottuk a Lando felépítését, ezért `$ lando rebuild -y`
7. Hogy biztosan felismerje az újonnnan hozzáadott repót: `$ (lando) composer clear-cache`
8. Ha eddig minden jól ment, akkor mostmár vissza tudjuk ellenőrizni, hogy sikerült-e a Composer szintjén a mutatvány:
    1. Kérjük először a legutolsó stabilt verziót, ahogy a modul projektoldalán is szerepel: `$ (lando) composer require 'drupal/acquia_purge:^1.3'`
    2. Ha sikerrel lefutott: `$ (lando) composer info drupal/acquia_purge --available`. A kimenetben csillaggal jelöli az aktuális verziót, de ami nekünk itt most fontosabb, az a „source” sor:
    ```bash
    […]
    source: [git] https://git.drupalcode.org/project/acquia_purge.git 8.x-1.3
    […]
    ```
    3. Most kérjük a saját fejlesztési águnkat a helyi Composer-repón keresztül: `$ (lando) composer require 'drupal/acquia_purge:dev-proba'`.
    4. Az infókat újra lekérdezve:
    ```bash
    […]
    source: [git] ./Repos/acquia_purge proba
    […]
    ```
    5. Ha át szeretnénk állni a nyilvános fejlesztési ágra (ami a mi helyi `packages.json`-ünkben nincs definiálva), akkor a Dev kiadás [saját oldalán](https://www.drupal.org/project/acquia_purge/releases/8.x-1.x-dev) feltüntetett módon kell igényelni: `$ (lando) composer require 'drupal/acquia_purge:1.x-dev@dev'` (és nem pedig a projektoldalon lent a kiadások szakasznál a _„Development version: 8.x-1.x-dev updated 31 May 2023 at 22:10 UTC”_ sorból kibányászni). Azt mondjuk nem teljesen értem, hogy az `acquia_purge` modul saját `composer.json`-jében ([kódban](https://git.drupalcode.org/project/acquia_purge/-/blob/b4a79956be20762b9019888db742a64cda008f4c/composer.json#L17)) miért van ág-alias definiálva. Természetesen az infólekérés ilyenkor is a Drupal központi Git-repójára fog mutatni a „source” sorban.
9. A fenti oda-vissza váltogatós mutatványt csak addig tudjuk megcsinálni tetszőlegesen kitalált lokális ágneveinkkel, amíg nem támaszkodik rájuk más. Ha ugyanis más csomagok is igénylik, akkor a Composer-nek rögtön fontossá válik, hogy összehasonlíthatóak legyenek a csomagok verziói. Ha egy kontrib modulnak egy MR-jének a saját ágára kell átállnom, akkor olyan ágnevet kapok, amit a Drupal GitLab-ja generál, ami biztosan nem lesz számszerűen összehasonlítható más verziószámokkal. Erre megoldás a Composer „branch alias”, azon belül is a mi jelen helyzetünkben „inline alias” ([doksi](https://getcomposer.org/doc/articles/aliases.md#require-inline-alias)) funkciója. Miután kikísérleteztem, így már nem nagy trükk az egész: `$ (lando) composer require "drupal/purge:dev-3397227-update-to-3.5 as 3.x-dev" -W`. Magyarul meg kell jelölni igényléskor, hogy a kért ág egyébként a `3.x` ágból ered, szóval nyugodjon meg, mindenki más, aki `3.*` verziót igényel ebből a csomagból, annak ez is jó lesz.
10. Ha már csomagok igénylésénél tartunk: a `--prefer-source` kapcsolót mindig használjuk, ha helyi fejlesztésre telepítünk kontrib modulokat. A modul első telepítésekor ezzel tudjuk a `.git` könyvtárát is kérni a Composer-től.
11. Ha eddig nem derült ki semmi gebasz, akkor valószínűleg egy `$ (lando) composer update --lock` sem fog semmi problémát mutatni.
12. Összefoglalva mindazt, ami a Composer terén eddig elhangzott: `$ (lando) composer require "drupal/purge:dev-3397227-update-to-3.5 as 3.x-dev" --prefer-source --update-with-all-dependencies`
13. Ekkor az `appserver` konténerébe belépve látnunk kell bentről mind a repónak (`$ ls -la /app/Repos/purge`), mind pedig magának a Drupal-modulnak (`$ ls -la /app/web/modules/contrib/purge`) a fájljait (korábban ugyanis előfordult, hogy csak a modul könyvtára jött létre, de üres volt, ami nyilván nem jó). Az egyetlen különbség az előbbi kettő között a macOS saját `.DS_Store` fájljának a jelenléte: az előbbiben azért van ott, mert az csak egy verbatim másolata (felmountolása) a gazdagépen lévő könyvtárnak, míg az utóbbi viszont már keresztülment a Composer saját logikáján és már csak azokat a fájlokat tartalmazza, amiket az adot csomag verziója tartalmaz.
14. Ha eddig eljutottunk, akkor még csak annyit értünk el, hogy meglehetősen körülményes módon, de sikerült a Drupal futtatásához szükséges kódot odavarázsolnunk a helyére. Ahhoz, hogy dolgozni tudjunk a kódjával, jó lenne, ha a Git-et is értesítenénk róla. A `.gitignore` fájlban ez a 4 sor szükséges, hogy benne legyen ([forrás](https://web.archive.org/web/20231110104638/https://stackoverflow.com/questions/5533050/gitignore-exclude-folder-but-include-specific-subfolder/18702826#18702826)):
    ```gitignore
    # Ezek a Drupal kontrib moduloknak a saját gépemen máshol tárolt forráskódjainak Docker-konténerbe behúzásához kellenek:
    /_my_local_packagist
    /Repos

    # A gyökér könyvtár, amit ki akarunk zárni:
    web/modules/contrib/**/*

    # És egy alkönyvtára, amit mégis követni szeretnénk:
    !web/modules/contrib/acquia_purge
    ```
15. A frissült `.gitignore` fájlt kommitoljuk el.
16. Ekkor érdemes az IDÉ-t (PhpStorm és VS Codium egyaránt) újraindítani, majd fel lehet venni a webhely projektjébe külön könyvtárként a fejlesztés alatt álló Drupal modult, így 2 külön Git-repót fog tudni kezelni: a webhelyét és a modulét (ugyebár azért, mert a 9. pontban a `--prefer-install=auto` kapcsolóval kértük a Composert, hogy a `.git` könyvtárával együtt klónozza oda a modult).
17. Ha nem felejtettem ki egyetlen lépést sem leírni, akkor _elvileg_ ilyenkor már a napi munka közben bármikor simán átválthatunk a modul másik verziójára a 12. pontban összefoglalt Composer-parancssal. Ha minden jól megy, akkor minden egyes átváltás után a Composer intelligenciája érvényesül: az újonnan használni kívánt Git-ágnak/-címkének megfelelően telepíti a függőségeket, foltozza amit kell, stb.



### A módszer hátrányai
- Manuálisan menedzselni kell a `_my_local_packagist/packages.json` fájl tartalmát. De annyira gyakran nem húzok le új kontrib modulokat, főleg nem túl gyakran nyitok bennük új ágakat, hogy ez nagy fáradság legyen.





----
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<!--▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇
▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇    ▇▇▇▇ -->
